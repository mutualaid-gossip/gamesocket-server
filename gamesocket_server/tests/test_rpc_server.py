"""
test_rpc_server
----------

Basic tests for gamesocket rpc server
"""

import asyncio
import json
import random

import zmq.asyncio

import gamesocket_server
from gamesocket_server.tests import base


class TestRPCServer(base.TestCase):

    hostname, base_port = '0.0.0.0', 10000

    def setUp(self):
        # NOTE: tests run in parallel
        super().setUp()
        self.port = self.base_port + random.randrange(2 << 9)

        self.loop = zmq.asyncio.ZMQEventLoop()
        asyncio.set_event_loop(self.loop)

        self.srv = self.loop.run_until_complete(gamesocket_server.create_server(self.port))

    def tearDown(self):
        super().tearDown()
        self.loop.run_until_complete(self.srv.wait_closed())
        self.loop.close()

    def test_setup_teardown(self):
        '''Exercises setUp and tearDown.'''

        async def go(loop):
            await asyncio.sleep(0.1)

        self.loop.run_until_complete(go(self.loop))

    def test_connect(self):

        async def go(loop):
            context = zmq.asyncio.Context()
            client = context.socket(zmq.REQ)
            endpoint = 'tcp://localhost:{}'.format(self.port)
            client.connect(endpoint)
            poller = zmq.asyncio.Poller()
            poller.register(client, zmq.POLLIN)

        self.loop.run_until_complete(go(self.loop))

    def test_method(self):
        '''Verify an error is returned when we call a non-existent method.'''

        async def go(loop):
            context = zmq.asyncio.Context()
            client = context.socket(zmq.REQ)
            endpoint = 'tcp://localhost:{}'.format(self.port)
            client.connect(endpoint)
            poller = zmq.asyncio.Poller()
            poller.register(client, zmq.POLLIN)
            payload = {'jsonrpc': '2.0', 'method': 'noop', 'id': 1}
            client.send_json(payload)
            timeout = 1000
            socks = dict(await poller.poll(timeout))
            assert socks.get(client) == zmq.POLLIN, socks.get(client)
            reply = json.loads((await client.recv()).decode())
            self.assertIsNotNone(reply.get('error'))

        self.loop.run_until_complete(go(self.loop))
