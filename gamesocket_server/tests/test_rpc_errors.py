"""
test_rpc_errors
----------

Exercise error handling.
"""

import asyncio
import json
import random

import zmq.asyncio

import gamesocket_server
from gamesocket_server.tests import base


class TestRPCErrors(base.TestCase):

    hostname, base_port = '0.0.0.0', 10000

    def setUp(self):
        # NOTE: tests run in parallel
        super().setUp()
        self.port = self.base_port + random.randrange(2 << 9)

        self.loop = zmq.asyncio.ZMQEventLoop()
        asyncio.set_event_loop(self.loop)

        self.srv = self.loop.run_until_complete(gamesocket_server.create_server(self.port))

        context = zmq.asyncio.Context()
        self.client = context.socket(zmq.REQ)
        endpoint = 'tcp://localhost:{}'.format(self.port)
        self.client.connect(endpoint)
        self.poller = zmq.asyncio.Poller()
        self.poller.register(self.client, zmq.POLLIN)

    def tearDown(self):
        super().tearDown()

        self.client.setsockopt(zmq.LINGER, 0)
        self.client.close()
        self.poller.unregister(self.client)

        self.loop.run_until_complete(self.srv.wait_closed())
        self.loop.close()

    def test_setup_teardown(self):
        '''Exercises setUp and tearDown.'''

        async def go(loop):
            await asyncio.sleep(0.1)

        self.loop.run_until_complete(go(self.loop))

    def test_maformed(self):

        async def go(loop):
            for _ in range(10):
                malformed = '''{'jsonrpc': '2.0, 'method:' 1 2 3 4}'''
                self.client.send_string(malformed)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertIn('error', reply)
                self.assertIsNotNone(reply.get('error'))

        self.loop.run_until_complete(go(self.loop))

    def test_invalid_request(self):

        async def go(loop):
            for _ in range(10):
                malformed = '''{'jsonrpc': '2.0, 'method:', 'missing_id'}'''
                self.client.send_string(malformed)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertIn('error', reply)
                self.assertIsNotNone(reply.get('error'))

        self.loop.run_until_complete(go(self.loop))
