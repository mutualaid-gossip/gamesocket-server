"""
test_server_minimal
----------

Test minimal server.
"""

import asyncio
import json
import random
import zmq.asyncio

import gamesocket_server.server
from gamesocket_server.tests import base


class MinimalServer(gamesocket_server.server.RPCServer):
    '''RPC server which exposes a single method.'''

    async def noop(self):
        return 0

    async def echo_args(self, arg1, arg2):
        return (arg1, arg2)


async def create_server(port):
    srv = MinimalServer()
    asyncio.ensure_future(srv.listen(port))
    return srv


class TestMinimal(base.TestCase):

    hostname, base_port = '0.0.0.0', 10000

    def setUp(self):
        # NOTE: tests run in parallel
        super().setUp()
        self.port = self.base_port + random.randrange(2 << 9)

        self.loop = zmq.asyncio.ZMQEventLoop()
        asyncio.set_event_loop(self.loop)

        self.srv = self.loop.run_until_complete(create_server(self.port))

        context = zmq.asyncio.Context()
        self.client = context.socket(zmq.REQ)
        endpoint = 'tcp://localhost:{}'.format(self.port)
        self.client.connect(endpoint)
        self.poller = zmq.asyncio.Poller()
        self.poller.register(self.client, zmq.POLLIN)

    def tearDown(self):
        super().tearDown()

        self.client.setsockopt(zmq.LINGER, 0)
        self.client.close()
        self.poller.unregister(self.client)

        self.loop.run_until_complete(self.srv.wait_closed())
        self.loop.close()

    def test_setup_teardown(self):
        '''Exercises setUp and tearDown.'''

        async def go(loop):
            await asyncio.sleep(0.1)

        self.loop.run_until_complete(go(self.loop))

    def test_method_noop(self):

        async def go(loop):
            for id in range(100):
                payload = {'jsonrpc': '2.0', 'method': 'noop', 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertNotIn('error', reply)
                self.assertIn('result', reply)
                self.assertIsNotNone(reply.get('result'))

        self.loop.run_until_complete(go(self.loop))

    def test_method_echo_args(self):

        async def go(loop):
            for id in range(100):
                payload = {'jsonrpc': '2.0', 'method': 'echo_args', 'params': [1, 2], 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertNotIn('error', reply)
                self.assertIn('result', reply)
                self.assertIsNotNone(reply.get('result'))
                self.assertEqual(reply.get('result'), [1, 2])

                payload = {'jsonrpc': '2.0', 'method': 'echo_args', 'params': {'arg2': 2, 'arg1': 1}, 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertNotIn('error', reply)
                self.assertIn('result', reply)
                self.assertIsNotNone(reply.get('result'))
                self.assertEqual(reply.get('result'), [1, 2])

        self.loop.run_until_complete(go(self.loop))

    def test_method_echo_args_missing_arg(self):

        async def go(loop):
            for id in range(100):
                payload = {'jsonrpc': '2.0', 'method': 'echo_args', 'params': [1], 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertIn('error', reply)
                self.assertIn('echo_args', reply['error']['message'])

                payload = {'jsonrpc': '2.0', 'method': 'echo_args', 'params': {'arg2': 2}, 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertIn('error', reply)
                self.assertIn('echo_args', reply['error']['message'])

        self.loop.run_until_complete(go(self.loop))

    def test_method_echo_args_incorrect_call(self):

        async def go(loop):
            for id in range(100):
                payload = {'jsonrpc': '2.0', 'method': 'echo_args', 'params': {'nonexistent_arg4': 2}, 'id': id}
                self.client.send_json(payload)
                timeout = 1000
                socks = dict(await self.poller.poll(timeout))
                assert socks.get(self.client) == zmq.POLLIN, socks.get(self.client)
                reply = json.loads((await self.client.recv()).decode())
                self.assertIn('error', reply)
                self.assertIn('echo_args', reply['error']['message'])

        self.loop.run_until_complete(go(self.loop))
