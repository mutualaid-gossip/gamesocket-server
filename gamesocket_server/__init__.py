import pbr.version

from .server import create_server, RPCServer  # noqa


__version__ = pbr.version.VersionInfo(
    'gamesocket_server').version_string()
