import abc
import asyncio
import functools
import json
import logging

import zmq.asyncio


logger = logging.getLogger(__name__)


async def create_server(port):
    srv = RPCServer()
    asyncio.ensure_future(srv.listen(port))
    return srv


def _format_result(result, sequence):
    '''Format result according to JSON-RPC 2.0 spec.'''
    return {'jsonrpc': '2.0', 'result': result, 'id': sequence}


class RPCServer(metaclass=abc.ABCMeta):
    '''Base class providing RPC functionality.

    It is expected that other classes will subclass this and define
    relevant methods.
    '''

    async def handle_request(self, request):
        if not all(key in request for key in {'jsonrpc', 'id', 'method'}) or request.get('jsonrpc') != '2.0':
            return {'jsonrpc': '2.0', 'error': {'code': -32600, 'message': 'Invalid Request'}, 'id': None}
        sequence, method_name, params = request['id'], request['method'], request.get('params', [])
        method = getattr(self, method_name, None)
        if method is None:
            return {'jsonrpc': '2.0', 'error': {'code': -32601, 'message': 'Method `{}` not found'.format(method_name)}, 'id': sequence}
        method = functools.partial(method, *params) if isinstance(params, list) else functools.partial(method, **params)
        try:
            result = await method()
        except Exception as exc:
            logger.error('Error in rpc call `{}`: {}'.format(method_name, repr(exc)), exc_info=exc)
            message = 'Error in `{}`: {}'.format(method_name, str(exc))
            return {'jsonrpc': '2.0', 'error': {'code': -32000, 'message': message}, 'id': sequence}
        return _format_result(result, sequence)

    async def listen(self, port):
        context = zmq.asyncio.Context()
        self._stop = asyncio.Event()

        self.server = context.socket(zmq.REP)
        self.server.bind("tcp://*:{}".format(port))
        poller = zmq.asyncio.Poller()
        poller.register(self.server, zmq.POLLIN)
        recv_timeout = 1000

        while not self._stop.is_set():
            socks = dict(await poller.poll(recv_timeout))
            if socks.get(self.server) != zmq.POLLIN:
                # self._stop.is_set() is True
                continue
            # zmq recv_json lacks asyncio support, need to call json.loads
            request_raw = (await self.server.recv()).decode()
            try:
                request = json.loads(request_raw)
            except ValueError:
                payload = {'jsonrpc': '2.0', 'error': {'code': -32700, 'message': 'Parse error'}, 'id': None}
                self.server.send_json(payload)
                continue
            result = await self.handle_request(request)
            self.server.send_json(result)

        self.server.close()
        context.term()
        logging.info('Server closed.')

    def close(self):
        logging.info('Backend got close request.')
        self._stop.set()

    async def wait_closed(self):
        self.close()
        while not self.server.closed:
            await asyncio.sleep(0.1)
