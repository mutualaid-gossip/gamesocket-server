============
Installation
============

At the command line::

    $ pip install gamesocket-server

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv gamesocket-server
    $ pip install gamesocket-server
