===============================
gamesocket-server
===============================

Respond to JSON-RPC requests

Please feel here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

Quickstart
----------

Subclass ``gamesocket_server.RPCServer`` and expose your own methods.

* Free software: Mozilla Public License

Features
--------

* TODO
